FROM continuumio/miniconda3

MAINTAINER Tobias Sebastian Finn <tobias.sebastian.finn@uni-hamburg.de

RUN apt-get update -q -y && \
    apt-get install -y build-essential git graphviz

SHELL ["/bin/bash", "-c"]
RUN conda update -n base conda
RUN git clone https://gitlab.com/tobifinn/slamodel.git
RUN conda env create -f /slamodel/dev_environment.yml
RUN source activate slamodel-dev && echo "Curr env: $CONDA_DEFAULT_ENV"
