Simplified Land-Atmosphere Model
================================

.. start_badges

.. list-table::
    :stub-columns: 1
    :widths: 15 85

    * - docs
      - |docs|
    * - pipeline
      - |pipeline|
    * - coverage
      - |coverage|
    * - quality
      - |quality|
    * - package
      - |pypi-test| |pypi| |conda|

.. |pipeline| image:: https://gitlab.com/tobifinn/slamodel/badges/dev/pipeline
.svg
    :target: https://gitlab.com/tobifinn/tobifinn/pipelines
    :alt: Pipeline status
.. |coverage| image:: https://gitlab.com/tobifinn/slamodel/badges/dev/coverage.svg
    :target: https://tobifinn.gitlab.io/slamodel/coverage-report/
    :alt: Coverage report
.. |docs| image:: https://img.shields.io/badge/docs-sphinx-brightgreen.svg
    :target: https://tobifinn.gitlab.io/slamodel/
    :alt: Documentation Status
.. |quality| image:: https://img.shields.io/badge/quality-codeclimate-brightgreen.svg
    :target: https://tobifinn.gitlab.io/slamodel/coverage-report/codeclimate.html
.. |pypi| image:: https://img.shields.io/badge/pypi-unavailable-lightgrey.svg
    :target: https://pypi.org/project/slamodel/
.. |pypi-test| image:: https://img.shields.io/badge/pypi_test-online-brightgreen.svg
    :target: https://test.pypi.org/project/slamodel/
.. |conda| image:: https://img.shields.io/badge/conda-unavailable-lightgrey.svg
    :target: https://anaconda.org/tobifinn/slamodel

.. end_badges

Single-column Simplified Land-Atmosphere Model
----------------------------------------------
This is a python-based Simplified single-column Land-Atmosphere Model.
This model is energy- and mass-conserving and uses a layered approach.
The model equations are derived from the "Community Land Model" [1],
"Consortium for Small-scale MOdelling" model [2] and the coupling of the
Terrestrial Systems Modelling Platform [3].
The programming is based on an object-oriented approach, where every model layer
is independent and has its own parametrizations.

This package is developed for a PhD-thesis about nonlinear methods in data
assimilation for the interface between atmosphere and land at the "Universität
Hamburg".

Installation
------------
We highly recommend to create a virtual environment for this package to prevent
package collisions.
At the moment this package is only available at pypi-test.

This package is programmed in python 3.7 and should be working with all `python
versions > 3.3`.

via conda (recommended):
^^^^^^^^^^^^^^^^^^^^^^^^
.. code:: sh

    git clone git@gitlab.com:tobifinn/slamodel.git
    cd slamodel
    conda env create -f environment.yml
    source activate slamodel
    pip install .

via pip (latest pypi-test):
^^^^^^^^
.. code:: sh

    pip install --index-url https://test.pypi.org/simple/ slamodel

Authors
-------
* **Tobias Finn** - *Initial creator* - `tobifinn <gitlab.com/tobifinn>`_

License
-------

This project is licensed under the GPL3 License - see the
`license <LICENSE.md>`_ file for details.

References
----------
.. [1] CLM, http://www.cesm.ucar.edu/models/clm/
.. [2] COSMO, http://www.cosmo-model.org/
.. [3] TerrSysMP, https://www.terrsysmp.org/
